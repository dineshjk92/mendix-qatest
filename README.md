# Assessment for test-engineer: 

## Introduction

This assessment consists of two assignments. Each assignment is intended to give
insights on your skills.

Please read the exercises carefully. If you have any questions at all, do not
hesitate to contact us.

After completing the exercises, please send us your results via email.

In your next interview we will discuss your results. The focus will be on your
reasoning and chosen methodologies when tackling the assignment. We will conclude
the interview with an exercise in exploratory testing.

Please treat this exercise as confidential so that we can reuse it for any
future candidates.

**Good luck and enjoy!**

---

# Part 1: Testing Mindset

See file: `pet-clinic.md`
 

# Part 2: Technical abillities

See file: `test-automation.md`

---

# Go make it!
We hope we'll be able to onboard you soon.
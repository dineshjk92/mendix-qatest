package SerenityAutomation.definitions;

import SerenityAutomation.stepsDescriptions.BrowserSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.*;

public class BasicDefinitions {

    @Steps
    private BrowserSteps browserSteps;

    @BeforeScenario
    public void openBrowserAndStart() {

    }

    @AfterScenario
    public void closeBrowserAndEndTest() {
        browserSteps.closeBrowser();
    }

    @Given("User opens the webpage")
    public void openStoryDefinedPage(@Named("pageUrl") String pageUrl) {
        browserSteps.openStoryDefinedPage(pageUrl);
    }

    @Then("user closes the browser")
    public void closeBrowser() {
        browserSteps.closeBrowser();
    }

    @Then("user closes the browser and deletes Cookies")
    public void closeBrowserAndDeleteCookies() {
        browserSteps.closeBrowserAndDeleteCookies();
    }
}
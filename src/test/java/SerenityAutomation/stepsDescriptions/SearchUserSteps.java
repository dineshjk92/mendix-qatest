package SerenityAutomation.stepsDescriptions;

import SerenityAutomation.methods.HomePageMethods;
import SerenityAutomation.methods.ProfilePageMethods;
import SerenityAutomation.methods.SearchResultPageMethods;
import SerenityAutomation.pages.HomePage;
import SerenityAutomation.pages.SearchResultPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SearchUserSteps {

    private HomePage homePage;
    private SearchResultPage searchResultPage;

    private static Logger logger = LogManager.getLogger(SearchUserSteps.class);


    @Step
    public boolean isUserMenuDisplayed() {
        return SearchResultPageMethods.isUserMenuDisplayed();
    }

    @Step
    public void searchForUser(String username) {
        HomePageMethods.searchUser(username);
    }

    @Step
    public void verifySearchResult() {
        Assert.assertTrue("No user profiles are found with the given username", SearchResultPageMethods.getUserCount() > 0);
    }

    @Step
    public void navigateToProfilePage() {
        try {
            SearchResultPageMethods.navigateToUserProfilePage();
            ProfilePageMethods.waitForProfilePageLoad();
        } catch (NoSuchElementException exception) {
            logger.info("Exceeded secondary rate limit. Hence navigating with direct url");
        }
    }

}
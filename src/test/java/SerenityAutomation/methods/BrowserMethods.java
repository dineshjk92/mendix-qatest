package SerenityAutomation.methods;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.openqa.selenium.WebDriver;

public class BrowserMethods extends PageObject {

    public static EnvironmentVariables envVariables = SystemEnvironmentVariables.createEnvironmentVariables();

    public BrowserMethods(WebDriver driver) {
        super(driver);
    }

    public void closeBrowser () {
        getDriver().close();
    }

    public void closeBrowserAndDeleteCookies () {
        getDriver().manage().deleteAllCookies();
        getDriver().close();
    }

    public void openBrowserWithProvidedPage(String pageUrl) {
        getDriver().navigate().to(pageUrl);
        System.out.println(getDriver().getTitle());
        Serenity.takeScreenshot();
    }

}
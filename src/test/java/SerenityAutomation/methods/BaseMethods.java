package SerenityAutomation.methods;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class BaseMethods extends PageObject {

    public static void clearAndType(WebElementFacade webElement, String inputText) {
        webElement.clear();
        webElement.type(inputText);
    }

    public static void waitAndClick(WebElementFacade webElement) {
        waitUntilVisible(webElement);
        webElement.click();
    }

    public static void waitUntilVisible(WebElementFacade webElement) {
        webElement.waitUntilVisible();
    }
}

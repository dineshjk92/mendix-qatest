package SerenityAutomation.methods;

import SerenityAutomation.pages.SearchResultPage;

public class SearchResultPageMethods extends BaseMethods {

    public static void waitForSearchResultPageLoad() {
        waitUntilVisible(SearchResultPage.userMenuItem);
    }

    public static boolean isUserMenuDisplayed() {
        return SearchResultPage.userMenuItem.isVisible();
    }

    public static int getUserCount() {
        return Integer
                .parseInt(SearchResultPage.userCount.getText());
    }
    public static void navigateToUserProfilePage() {
        waitAndClick(SearchResultPage.userMenuItem);
        waitAndClick(SearchResultPage.userProfileLink);
    }


}